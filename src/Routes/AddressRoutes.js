const AddressController = require('../Controllers/AddressController');

module.exports = (app) => {
  app.get('/address/:cep', AddressController.get);
};
