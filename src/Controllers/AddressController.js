const isValidCep = require('@brazilian-utils/is-valid-cep');
const axios = require('axios');

exports.get = async (req, res, next) => {
  const { cep } = req.params;

  if (!isValidCep(cep)) {
    res.status(500).send({ error: 'Invalid CEP' });
  }

  axios
    .get(`https://viacep.com.br/ws/${cep}/json/`)
    .then(async (response) => {
      res.status(200).send(response.data);
    })
    .catch((err) => {
      console.log('error =>', err);
      res.status(500).send();
    });
};
