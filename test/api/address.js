/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable no-undef */
const { expect } = require('chai');
const request = require('supertest');
const app = require('../../server');

describe('Address API', () => {
  it('Should return CEP Info', (done) => {
    request(app)
      .get('/address/65907260')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        const addressInfo = res.body;

        expect(addressInfo)
          .to.be.an('object')
          .that.have.all.keys(
            'cep',
            'logradouro',
            'complemento',
            'bairro',
            'localidade',
            'uf',
            'ibge',
            'gia',
            'ddd',
            'siafi'
          );
        done();
      });
  });

  it('Should return error for invalid CEP', (done) => {
    request(app)
      .get('/address/659072')
      .expect('Content-Type', /json/)
      .expect(500)
      .end(() => {
        done();
      });
  });
});
